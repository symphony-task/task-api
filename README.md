# task-api-front

This project contains back-end Cypress automation tests. The Cucumber framework was used for documentation.
Used GitLab CI/CD pipeline.

## To run the project locally

Install the dependencies. Once you have cloned the project, run the command "npm install cypress" in your terminal. Then, run the command "npx cypress run" to run the project in headless mode or "npx cypress open" to run the project with an interface.

## To run the project on GitLab

Go to https://gitlab.com/symphony-task/task-api/-/pipelines.
Click on "Run Pipeline" button

## Init project

npm -i init

## Install Cypress

npm install cypress

## Open Cypress

npx cypress open

## Cypress terminal

npx cypress run

