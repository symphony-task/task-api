@api
Feature: Fake Store API
Scenario: Search all products
  Given I make a request to the API of products
  Then Status Code 200

Scenario: Search product category
  Given I make a request to the API of product category
  Then Status Code 200

Scenario Outline: Search product by category
  Given I make a request to the API of product by "<category>"
  Then Status Code <status_code>

  Examples:
  |category        |status_code|
  |jewelery        |200        |
  |electronics     |200        |
  |men's clothing  |200        |
  |women's clothing|200        |

Scenario Outline: Product search by ID
    Given I make a request to the API with <id_product>
    Then the response returns the correct "<product_title>"
    And Status Code <status_code>

    Examples:
    |id_product|product_title                                        |status_code|
    |1         |Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops|200        |
    |10        |SanDisk SSD PLUS 1TB Internal SSD - SATA III 6 Gb/s  |200        |
    |20        |DANVOUY Womens T Shirt Casual Cotton Short           |200        |

