import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";

let apiResponse;
let responseObject;

Given("I make a request to the API of products", () => {
    cy.request('GET', 'https://fakestoreapi.com/products').as('apiResponse');
 })

Given("I make a request to the API of product category", () => {
    cy.request('GET', 'https://fakestoreapi.com/products/categories').as('apiResponse');
 })

 Given("I make a request to the API of product by {string}", category => {
    cy.request('GET', 'https://fakestoreapi.com/products/category/' + category).as('apiResponse');
 }) 

Given("I make a request to the API with {int}", idProduct => {
   cy.request('GET', 'https://fakestoreapi.com/products/'+ idProduct).as('apiResponse');
})

Then("the response returns the correct {string}", productTitle => {
    cy.get('@apiResponse').its('body').should('have.property', 'title', productTitle);
 })

And("Status Code {int}", statusCode => {
    cy.get('@apiResponse').its('status').should('equal', statusCode);
})

